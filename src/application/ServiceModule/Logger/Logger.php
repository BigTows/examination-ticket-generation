<?php
/**
 * Created by PhpStorm.
 * User: a.chapchuk
 * Date: 14.11.2017
 * Time: 10:27
 */

namespace Ticket\ServiceModule;

/**
 * Данный класс помогает выводить ошибки в лог файле, и так же пользователю
 * @package Ticket\ServiceModule
 */
class Logger
{

    public static function info (string $message){
        self::__logIntoFile($message);
    }

    /**
     * Логгинрование при ошибках
     * @param \Exception $exception
     */
    public static function onException(\Exception $exception){

    }

    /**
     * @param \Exception $exception
     */
    public static function onCriticalException(\Exception $exception){
        self::__logIntoFile("[EXCEPTION]".$exception->getMessage());
        //TODO Smarty load 500 error
    }


    private static function __logIntoFile($message){
        echo $message;
    }
}