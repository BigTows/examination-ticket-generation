<?php
/**
 * Created by PhpStorm.
 * User: a.chapchuk
 * Date: 14.11.2017
 * Time: 10:27
 */

namespace Ticket\ServiceModule;


class ServiceModule
{
    private static $smarty = null;

    public static function getSmarty()
    {
        return self::$smarty === null ? self::__initSmarty() : self::$smarty;
    }

    private static function __initSmarty()
    {
        return new Smarty();
    }
}