<?php

namespace Ticket\DataBase\Entity;

interface InterfaceEntity {
    /**
     * Получение Id
     * @return int
     */
    public function getId(): int;

}