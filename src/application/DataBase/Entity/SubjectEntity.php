<?php
/**
 * Created by PhpStorm.
 * File: SubjectEntity.php.
 * Created: bigtows.
 * Created date: 13/11/2017  22:48
 * Description:
 */

namespace Ticket\DataBase\Entity;


class SubjectEntity extends AbstractEntity {

    /**
     * @var string Название предмета
     */
    private $name;

    function __construct($id, $name) {
        $this->setId($id);
        $this->setName($name);
    }

    /**
     * Получение название предмета
     * @return string
     */
    function getName(): string {
        return $this->name;
    }

    /**
     * Установить имя
     * @param string $name
     */
    protected function setName(string $name){
        $this->name = $name;
    }
}