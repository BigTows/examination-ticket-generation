<?php
/**
 * Created by PhpStorm.
 * User: a.chapchuk
 * Date: 15.11.2017
 * Time: 13:43
 */

namespace Ticket\DataBase\Entity;


class ChairmanEntity extends AbstractEntity
{

    private $firstName;
    private $secondName;
    private $middleName;

    /**
     * ChairmanEntity constructor.
     * @param $id
     * @param $firstName
     * @param $secondName
     * @param $middleName
     */
    public function __construct($id, $firstName, $secondName, $middleName)
    {
        $this->setId($id);
        $this->setFirstName($firstName);
        $this->setSecondName($secondName);
        $this->setMiddleName($middleName);
    }


    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    protected function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * @param mixed $secondName
     */
    protected function setSecondName($secondName)
    {
        $this->secondName = $secondName;
    }

    /**
     * @return mixed
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param mixed $middleName
     */
    protected function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

}