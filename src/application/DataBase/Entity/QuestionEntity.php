<?php
/**
 * Created by PhpStorm.
 * User: a.chapchuk
 * Date: 15.11.2017
 * Time: 13:53
 */

namespace Ticket\DataBase\Entity;


class QuestionEntity extends AbstractEntity
{
    /**
     * @var SubjectEntity
     */
    private $subject;

    /**
     * @var ChairmanEntity
     */
    private $chairman;

    /**
     * QuestionEntity constructor.
     * @param int $id
     * @param SubjectEntity $subject
     * @param ChairmanEntity $chairman
     */
    public function __construct(int $id,SubjectEntity $subject, ChairmanEntity $chairman)
    {
        $this->setId($id);
        $this->subject = $subject;
        $this->chairman = $chairman;
    }


    /**
     * @return SubjectEntity
     */
    public function getSubject(): SubjectEntity
    {
        return $this->subject;
    }

    /**
     * @param SubjectEntity $subject
     */
    protected function setSubject(SubjectEntity $subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return ChairmanEntity
     */
    public function getChairman(): ChairmanEntity
    {
        return $this->chairman;
    }

    /**
     * @param ChairmanEntity $chairman
     */
    protected function setChairman(ChairmanEntity $chairman)
    {
        $this->chairman = $chairman;
    }


}