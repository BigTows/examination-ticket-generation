<?php
/**
 * Created by PhpStorm.
 * User: a.chapchuk
 * Date: 15.11.2017
 * Time: 13:43
 */

namespace Ticket\DataBase\Entity;


abstract class AbstractEntity implements InterfaceEntity
{
    private $id;

    public function getId(): int
    {
        return $this->id;
    }

    protected function setId(int $id)
    {
        $this->id = $id;
    }
}