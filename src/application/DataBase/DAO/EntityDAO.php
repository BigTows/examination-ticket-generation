<?php
/**
 * Created by PhpStorm.
 * User: a.chapchuk
 * Date: 15.11.2017
 * Time: 13:55
 */

namespace Ticket\DataBase\DAO;


use Ticket\DataBase\Entity\InterfaceEntity;

interface EntityDAO
{
    /**
     * @return InterfaceEntity[]
     */
    public static function getAll(): array;

    public static function getById(int $id): InterfaceEntity;
}