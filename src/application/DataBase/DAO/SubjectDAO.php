<?php
/**
 * Created by PhpStorm.
 * User: a.chapchuk
 * Date: 15.11.2017
 * Time: 13:55
 */

namespace Ticket\DataBase\DAO;


use Ticket\Application;
use Ticket\DataBase\Entity\InterfaceEntity;
use Ticket\DataBase\Entity\SubjectEntity;

class SubjectDAO implements EntityDAO
{

    public static function getById(int $id): InterfaceEntity
    {
        $query = "SELECT * FROM Subject WHERE id = ?";
        $statement = Application::getDB()->getConnection()->prepare($query);
        $statement->execute([$id]);
        foreach ($statement as $row) {
            return new SubjectEntity($row['id'], $row['name']);
        }
        return null;
    }

    /**
     * @return InterfaceEntity[]
     */
    public static function getAll(): array
    {
        $query = "SELECT * FROM Subject";
        $statement = Application::getDB()->getConnection()->query($query);
        $result = [];
        foreach ($statement as $row) {
            array_push($result, new SubjectEntity($row['id'], $row['name']));
        }
        return $result;
    }
}