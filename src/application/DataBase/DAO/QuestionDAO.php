<?php
/**
 * Created by PhpStorm.
 * User: a.chapchuk
 * Date: 15.11.2017
 * Time: 14:06
 */

namespace Ticket\DataBase\DAO;


use Ticket\DataBase\Entity\InterfaceEntity;
use Ticket\Application;
use Ticket\DataBase\Entity\ChairmanEntity;

class QuestionDAO implements EntityDAO
{

    /**
     * @return InterfaceEntity[]
     */
    public static function getAll(): array
    {
        $query = "SELECT * FROM Question";
        $statement = Application::getDB()->getConnection()->query($query);
        $result = [];
        foreach ($statement as $row) {

            array_push($result, new ChairmanEntity($row['id'],
                $row['firstName'],
                $row['secondName'],
                $row['middleName']
            ));
        }
        return $result;
    }

    public static function getById(int $id): InterfaceEntity
    {
        // TODO: Implement getById() method.
    }
}