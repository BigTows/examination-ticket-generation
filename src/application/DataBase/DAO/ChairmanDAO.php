<?php
/**
 * Created by PhpStorm.
 * User: a.chapchuk
 * Date: 15.11.2017
 * Time: 14:04
 */

namespace Ticket\DataBase\DAO;


use Ticket\Application;
use Ticket\DataBase\Entity\ChairmanEntity;
use Ticket\DataBase\Entity\InterfaceEntity;

class ChairmanDAO implements EntityDAO
{

    /**
     * @return InterfaceEntity[]
     */
    public static function getAll(): array
    {
        $query = "SELECT * FROM Chairman";
        $statement = Application::getDB()->getConnection()->query($query);
        $result = [];
        foreach ($statement as $row) {
            array_push($result, new ChairmanEntity($row['id'],
                $row['firstName'],
                $row['secondName'],
                $row['middleName']
            ));
        }
        return $result;
    }

    public static function getById(int $id): InterfaceEntity
    {
        $query = "SELECT * FROM Chairman WHERE id = ?";
        $statement = Application::getDB()->getConnection()->prepare($query);
        $statement->execute([$id]);
        foreach ($statement as $row) {
            return new ChairmanEntity($row['id'],
                $row['firstName'],
                $row['secondName'],
                $row['middleName']
            );
        }
        return null;
    }
}