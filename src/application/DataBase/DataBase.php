<?php
/**
 * Created by PhpStorm.
 * File: DataBase.php.
 * Created: bigtows.
 * Created date: 13/11/2017  22:52
 * Description:
 */

namespace Ticket\DataBase;


use Ticket\ServiceModule\Logger;

class DataBase {

    /**
     * @var \PDO
     */
    private $connection;

    function __construct(DataBaseConfig $config) {
        try {
            $this->connection = new \PDO("mysql:host={$config->getHost()};dbname={$config->getDbName()}", $config->getName(), $config->getPassword());
        }catch (\PDOException $exception){
            Logger::onCriticalException($exception);
        }
    }


    private function testConnection(){
        $this->connection->query();
    }

    public function getConnection(){
        return $this->connection;
    }

}