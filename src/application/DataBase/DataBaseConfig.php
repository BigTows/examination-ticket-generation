<?php
/**
 * Created by PhpStorm.
 * File: ${FILE_NAME}.
 * Created: bigtows.
 * Created date: 13/11/2017  22:54
 * Description:
 */

namespace Ticket\DataBase;


class DataBaseConfig {

    private $host;
    private $port = 3306;
    private $name = "root";
    private $password;
    private $dbName = "Tickets";
    /**
     * DataBaseConfig constructor.
     * @param $host
     * @param int $port
     * @param string $name
     * @param $password
     */
    public function __construct($host = "localhost", $name = "root", $password = "", $port = 3306) {
        $this->host = $host;
        $this->port = $port;
        $this->name = $name;
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getHost() {
        return $this->host;
    }

    /**
     * @return int
     */
    public function getPort(): int {
        return $this->port;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getDbName(): string
    {
        return $this->dbName;
    }




}